# CICD utilizando gitlab + GCP BASTIONHOST

- criar uma instancia linux
- editar
- segurança e acesso
- chave SSH
- utilizar puttygen
- generate public key (mudar nome de usuario no fim do arquivo = gcp)
- save private key

## logar

```
gcloud config set account nomedeusuarionogcp
gcloud init
```

## Liberar acesso ao cluster

gcloud auth login

- copiar codigo e logar

## Instalar dependencias na BASTION

sudo apt-get install kubectl google-cloud-sdk-gke-gcloud-auth-plugin git

## Converter chave ppk para pem

- Abrir arquivo e copiar conteudo da chave.
- Adicionar variavel ao gitlab SSH_KEY
- Adicionar variavel ao gitlab SSH_SERVER
